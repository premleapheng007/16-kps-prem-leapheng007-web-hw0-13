import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Header from "./component/Header";
import Contant from "./component/Contant";
import AddArtical from "./component/AddArtical";
import { Switch, Route } from "react-router-dom";
import { BrowserRouter as Router } from "react-router-dom";
import ViewArtical from "./component/ViewArtical";
import Axios from "axios";
import Edit from "./component/Edit";

export default class App extends Component {
  constructor() {
    super();

    this.state = {
      artical: [],
    };
  }
  componentDidMount() {
    Axios.get(
      "http://110.74.194.124:15011/v1/api/articles?page=1&limit=10"
    ).then((res) => {
      this.setState({
        artical: res.data.DATA,
      });
    });
  }
  componentDidUpdate() {
    Axios.get(
      "http://110.74.194.124:15011/v1/api/articles?page=1&limit=10"
    ).then((res) => {
      this.setState({
        artical: res.data.DATA,
      });
    });
  }
  delete = (id) => {
    this.setState({
      artical: [...this.state.artical.filter((artical) => artical.ID !== id)],
    });
    Axios.delete(`http://110.74.194.124:15011/v1/api/articles/${id}`).then(
      (res) => {
        alert(res.data.MESSAGE);
      }
    );
  };
  convertDate = (dateStr) => {
    var dateString = dateStr;
    var year = dateString.substring(0, 4);
    var month = dateString.substring(4, 6);
    var day = dateString.substring(6, 8);
    var date = year + "-" + month + "-" + day;
    return date;
  };
  render() {
    console.log();
    return (
      <div>
        <Router>
          <Header />
          <Switch>
            <Route exact path="/">
              <Contant
                artical={this.state.artical}
                onDelete={this.delete}
                formatDate={this.convertDate}
                viewData={this.viewData}
              />
            </Route>
            <Route path="/add" component={AddArtical}></Route>
            <Route
              path="/update/:id"
              render={(props) => (
                <Edit {...props} artical={this.state.artical} />
              )}
            ></Route>
            <Route
              path="/view/:id"
              render={(props) => (
                <ViewArtical {...props} artical={this.state.artical} />
              )}
            ></Route>
          </Switch>
        </Router>
      </div>
    );
  }
}
