import React, { Component } from "react";
import axios from "axios";
import { Row, Form, Button } from "react-bootstrap";
import IMAGE from "./image/DefaultIMG.jpg";

export default class Edit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      TITLE: "",
      DESCRIPTION: "",
      IMAGE: "",
    };
  }
  onChangeUpdate(e) {
    e.preventDefault();
    const Update = {
      TITLE: this.state.TITLE,
      DESCRIPTION: this.state.DESCRIPTION,
      IMAGE: this.state.IMAGE,
    };
    const ID = this.props.match.params.id;
    axios
      .put(`http://110.74.194.124:15011/v1/api/articles/${ID}`, Update)
      .then((res) => {
        console.log(res.data);
        alert(res.data.MESSAGE);
        window.history.back();
      });
  }
  onImageChange = (e) => {
    console.log(e);
    let image = document.getElementById("imgID");
    image.src = URL.createObjectURL(e.target.files[0]);
    if (e.target.files && e.target.files[0]) {
      this.setState({
        IMAGE: URL.createObjectURL(e.target.files[0]),
      });
    }
  };
  componentDidMount() {
    const ID = this.props.match.params.id;
    axios
      .get(`http://110.74.194.124:15011/v1/api/articles/${ID}`)
      .then((res) => {
        this.setState({
          data: res.data.DATA,
          TITLE: res.data.DATA.TITLE,
          DESCRIPTION: res.data.DATA.DESCRIPTION,
          IMAGE: res.data.DATA.IMAGE,
        });
      });
  }
  changeHandler = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };
  render() {
    return (
      <div className="container">
        <Form onSubmit={(e) => this.onChangeUpdate(e)}>
          <h3>Udate Articles</h3>
          <Row>
            <div className="col-lg-8 col-md-8 col-sm-12">
              <Form.Group controlId="formGroupTitle">
                <Form.Label>TITLE</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter title"
                  name="TITLE"
                  value={this.state.TITLE}
                  onChange={(e) => this.changeHandler(e)}
                />
              </Form.Group>
              <Form.Group controlId="formGroupDescription">
                <Form.Label>DESCRIPTION</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter description"
                  name="DESCRIPTION"
                  value={this.state.DESCRIPTION}
                  onChange={(e) => this.changeHandler(e)}
                />
              </Form.Group>
              <Button
                variant="primary"
                type="submit"
                onChange={this.onChangeUpdate}
              >
                Update
              </Button>
            </div>
            <div className="col-lg-4 col-md-4 col-sm-12">
              <Form.Group>
                <img
                  id="imgID"
                  src={this.state.IMAGE}
                  style={{
                    width: "100%",
                    height: "200px",
                    position: "absolute",
                  }}
                  onChange={this.onImageChange}
                />
                {/* <Form.File id="exampleFormControlFile1" onChange={this.onImageChange} /> */}
                <input
                  type="file"
                  onChange={this.onImageChange}
                  style={{
                    width: "100%",
                    height: "400px",
                    position: "relative",
                    opacity: "0",
                  }}
                />
              </Form.Group>
            </div>
          </Row>
        </Form>
      </div>
    );
  }
}
