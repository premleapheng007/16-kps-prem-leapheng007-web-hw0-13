import React, { Component } from "react";
import { Form, Button, Card } from "react-bootstrap";
import Axios from "axios";
import Img from "./image/DefaultIMG.jpg";

export default class AddArtical extends Component {
  constructor(props) {
    super(props);

    this.state = {
      msg: "",
      myimg: "",
      article: { title: "", description: "", image: "" },
    };
  }
  getTitle(event) {
    this.setState({
      article: {
        ...this.state.article,
        title: event.target.value,
      },
    });
  }
  getDes(event) {
    this.setState({
      article: {
        ...this.state.article,
        description: event.target.value,
      },
    });
  }
  onImageChange = (event) => {
    if (event.target.files && event.target.files[0]) {
      this.setState({
        myimg: URL.createObjectURL(event.target.files[0]),
      });
    }
  };
  Summit() {
    let article = {
      TITLE: this.state.article.title,
      DESCRIPTION: this.state.article.description,
      IMAGE: this.state.myimg,
    };
    Axios.post("http://110.74.194.124:15011/v1/api/articles", article).then(
      (res) => {
        // console.log(res.data.MESSAGE);
        // this.setState({msg:res.data.MESSAGE})
        alert(res.data.MESSAGE);
        // console.log(res.data.DATA)
      }
    );
    this.props.history.push("/");
  }

  render() {
    return (
      <div className="container mt-3">
        <h3>Add Artical</h3>
        <div className="row">
          <div className="col-xl-8">
            <Form>
              <Form.Group controlId="formBasicEmail">
                <Form.Label>TITLE</Form.Label>
                <Form.Control
                  placeholder="Title"
                  onChange={this.getTitle.bind(this)}
                />
              </Form.Group>

              <Form.Group controlId="formBasicPassword">
                <Form.Label>DESCRIPTION</Form.Label>
                <Form.Control
                  onChange={this.getDes.bind(this)}
                  placeholder="Description"
                />
              </Form.Group>
              <Button variant="warning" onClick={this.Summit.bind(this)}>
                Submit
              </Button>
            </Form>
          </div>
          <div className="col-xl-4">
            <Card.Img
              id="imgID"
              src={this.state.myimg == "" ? Img : this.state.myimg}
              style={{ width: "100%", height: "200px", position: "absolute" }}
            />
            <input
              type="file"
              onChange={this.onImageChange}
              style={{
                width: "100%",
                height: "400px",
                position: "relative",
                opacity: "0",
              }}
            />
          </div>
        </div>
      </div>
    );
  }
}
