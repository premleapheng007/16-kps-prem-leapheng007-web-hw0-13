import React, { Component } from "react";
import { Button, Card } from "react-bootstrap";
import { Link } from "react-router-dom";
export default class DataTable extends Component {
  render() {
    console.log();
    return (
      <tr key={this.props.data.ID}>
        <td>{this.props.data.ID}</td>
        <td>{this.props.data.TITLE}</td>
        <td style={{ margin: "auto", with: "100px" }}>
          {this.props.data.DESCRIPTION}
        </td>
        <td>{this.props.formatDate(this.props.data.CREATED_DATE)}</td>
        <td style={{ padding: "0px" }}>
          <Card.Img
            variant=""
            src={this.props.data.IMAGE}
            style={{ width: "100px", height: "100px", borderRadius: "0px" }}
          />
        </td>
        <td style={{ margin: "auto", with: "400px" }}>
          <div style={{ margin: "auto" }}>
            <Button
              as={Link}
              to={`/view/${this.props.data.ID}`}
              variant="primary"
            >
              View
            </Button>{" "}
            <Button
              as={Link}
              to={`/update/${this.props.data.ID}`}
              variant="warning"
            >
              Update
            </Button>{" "}
            <Button
              variant="danger"
              onClick={() => this.props.onDelete(this.props.data.ID)}
            >
              Delete
            </Button>{" "}
          </div>
        </td>
      </tr>
    );
  }
}
