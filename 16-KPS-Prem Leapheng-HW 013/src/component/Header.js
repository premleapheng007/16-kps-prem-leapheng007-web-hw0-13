import React, { Component } from "react";
import {
  Nav,
  Form,
  FormControl,
  Navbar,
  Container,
  // Button,
} from "react-bootstrap";
import {Link} from 'react-router-dom'

export default class Header extends Component {
  render() {
    return (
      <div>
        <Navbar bg="light " expand="lg">
          <Container>
            <Navbar.Brand as={Link} to="/" style={{ fontSize: "30" }}>
              AMS
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="mr-auto">
                <Nav.Link as={Link} to="/" >Home</Nav.Link>
              </Nav>
              <Form inline>
                <FormControl
                  type="text"
                  placeholder="Search"
                  className="mr-sm-2"
                />
                {/* <Button variant="outline-success b-danger">Search</Button> */}
              </Form>
            </Navbar.Collapse>
          </Container>
        </Navbar>
      </div>
    );
  }
}
