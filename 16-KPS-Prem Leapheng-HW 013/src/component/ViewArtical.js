import React from "react";
import { Card } from "react-bootstrap";

export default function ViewArtical({ match, artical }) {
  let ViewArtical = artical.find((artical) => artical.ID == match.params.id);
  return (
    <div className="container mt-4">
      <div className="row">
        <div className="col-xl-4">
          <Card.Img
            variant=""
            src={ViewArtical.IMAGE}
            style={{ width: "100%", height: "200px" }}
          />
        </div>
        <div className="col-xl-8">
          <h5 style={{fontWeight:"bold"}}>{ViewArtical.TITLE}</h5>
          <p>{ViewArtical.DESCRIPTION}</p>
        </div>
      </div>
    </div>
  );
}
