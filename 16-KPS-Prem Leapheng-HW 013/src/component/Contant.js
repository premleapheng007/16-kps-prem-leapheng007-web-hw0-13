import React, { Component } from "react";
import { Button, Table, Container } from "react-bootstrap";
import { Link } from "react-router-dom";
import DataTable from "./DataTable";

export default class Contant extends Component {
  render() {
    let articals = this.props.artical.map((data) => (
      <DataTable
        key={data.ID}
        data={data}
        onDelete={this.props.onDelete}
        formatDate={this.props.formatDate}
        viewData={this.props.viewData}
      />
    ));

    return (
      <div>
        <div style={{ textAlign: "center" }} className="mt-4">
          <h3>Artical Managerment</h3>
          <Button as={Link} to="/add" className="mt-4" variant="dark">
            Add New Artical
          </Button>
        </div>
        <div className="mt-4">
          <Container>
            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>ID</th>
                  <th>TITLE</th>
                  <th>DESCRIPTION</th>
                  <th style={{ with: "200px" }}>CREATE DATE</th>
                  <th style={{ with: "15%" }}>IMAGE</th>
                  <th style={{ width: "244px" }}>ACTION</th>
                </tr>
              </thead>
              <tbody>{articals}</tbody>
            </Table>
          </Container>
        </div>
      </div>
    );
  }
}
